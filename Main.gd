extends Node2D

var playerRef
var gateRef
var isGameOver

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	playerRef = get_node('Player')
	gateRef = get_node('Gate')
	isGameOver = false

func checkGameOver()->void:
	if (!playerRef.isAlive or !gateRef.isAlive) and !isGameOver:
		$GameOver.play()
		isGameOver = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	checkGameOver()
