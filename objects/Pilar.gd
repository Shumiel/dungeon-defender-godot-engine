extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.animation = "red"
	$AnimatedSprite2.animation = "red"


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Pilar_area_entered(area):
	if area.name == "Player":
		$AnimatedSprite.animation = "blue"
		$AnimatedSprite2.animation = "blue"


func _on_Pilar_area_exited(area):
	if area.name == "Player":
		$AnimatedSprite.animation = "red"
		$AnimatedSprite2.animation = "red"
