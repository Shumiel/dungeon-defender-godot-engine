extends Area2D


# Declare member variables here. Examples:
export var speed = 160
export var bounceEffect = 90
export var isAlive = true
export var arrows = 3
var screen_size

onready var stun_timer: Timer = $StunTimer
onready var tween: Tween = $MoveTween
onready var life_timer: Timer = $LifeTimer
onready var death_timer: Timer = $DeathTimer

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var velocity = Vector2()  # The player's movement vector.
	isAlive = $LifeBar.value > 0
	if stun_timer.is_stopped() && isAlive:
		if Input.is_action_pressed("ui_right"):
			velocity.x += 1
		if Input.is_action_pressed("ui_left"):
			velocity.x -= 1
		if Input.is_action_pressed("ui_down"):
			velocity.y += 1
		if Input.is_action_pressed("ui_up"):
			velocity.y -= 1
		if velocity.length() > 0:
			velocity = velocity.normalized() * speed
			$AnimatedSprite.play()
		
		
	if velocity.x != 0:
		$AnimatedSprite.animation = "run"
		$AnimatedSprite.flip_v = false
		$AnimatedSprite.flip_h = velocity.x < 0
	elif velocity.y != 0:
		$AnimatedSprite.animation = "run"
	else:
		if stun_timer.is_stopped():
			if $LifeBar.value <= 0:
				$AnimatedSprite.animation = "die"
				$AnimatedSprite.playing = false
			else:
				$AnimatedSprite.animation = "default"
		else:
			$AnimatedSprite.animation = "roll"
	
	position += velocity * delta
	position.x = clamp(position.x, 32, screen_size.x - 32)
	position.y = clamp(position.y, 32, screen_size.y - 64)

#stun duration function used between collisions player<->enemy
func stun(duration: = 0.3)->void:
	stun_timer.wait_time = duration
	stun_timer.start()

#logic for player life
func checkPlayerLife(duration: = 1.0, damaged: = 25)->void:
	$LifeBar.visible = true
	life_timer.wait_time = duration
	life_timer.start()
	$LifeBar.value -= damaged
	if ($LifeBar.value <= 0):
		death_timer.wait_time = 4
		death_timer.start()

#knock back function used for player collision with enemy
func knock_back(direction: Vector2)->void:
	var distance = bounceEffect
	tween.interpolate_property(self, "position", position, position + direction * distance, 0.3, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.start()

#logic for Player being collided with enemy
func _on_Player_area_entered(area: Area2D)->void:
	if area.name == 'Enemy':
		$Damaged.play()
		var knock_direction = (position - area.position).normalized()
		stun()
		checkPlayerLife()
		knock_back(knock_direction)

#after timeout hide Player life again
func _on_LifeTimer_timeout():
	$LifeBar.visible = false

#after timeout clear tree and start the game again
func _on_DeathTimer_timeout():
	get_tree().reload_current_scene()
