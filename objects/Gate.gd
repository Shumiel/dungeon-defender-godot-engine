extends Area2D

onready var life_timer: Timer = $LifeTimer
export var isAlive = true

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func checkGateLife()->void:
	if $GateLife.value <= 0 and isAlive:
		isAlive = false
		print('Game Over')
		life_timer.wait_time = 4
		life_timer.start()

func _on_LifeTimer_timeout():
	get_tree().reload_current_scene()


func _on_Gate_area_entered(area):
	if area.name != 'Player':
		$GateLife.value -= 25
		$Damaged.play()
		checkGateLife()
