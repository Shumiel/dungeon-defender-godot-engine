extends Area2D

# Declare member variables here. Examples:
export var speed = 100
export var life = 250
export var bounceEffect = 80
var screen_size
var playerRef
var gateRef
var playerDistanceX
var playerDistanceY
var gateDistanceX
var gateDistanceY

onready var stun_timer: Timer = $StunTimer
onready var tween: Tween = $MoveTween
onready var life_timer: Timer = $LifeTimer

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	playerRef = get_parent().get_node('Player')
	gateRef = get_parent().get_node('Gate')

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var velocity = Vector2()  # The player's movement vector.
	
	#player distance
	playerDistanceX = playerRef.position.x - position.x
	playerDistanceY = playerRef.position.y - position.y
	#gate distance
	gateDistanceX = gateRef.position.x - position.x
	gateDistanceY = gateRef.position.y - position.y
	var playerXY = abs(playerDistanceX + playerDistanceY)
	var gateXY = abs(gateDistanceX + gateDistanceY)
	
	if (playerXY > gateXY) and gateRef.isAlive:
		#print('Gate', gateXY)
		if (gateRef.position.x > position.x):
			velocity.x += 1
		elif (gateRef.position.x < position.x):
			velocity.x -= 1
			
		if (gateRef.position.y > position.y):
			velocity.y += 1
		elif (gateRef.position.y < position.y):
			velocity.y -= 1	
	elif (playerXY < gateXY) and playerRef.isAlive:
		#print('Player', playerXY)
		if (playerRef.position.x > position.x):
			velocity.x += 1
		elif (playerRef.position.x < position.x):
			velocity.x -= 1
			
		if (playerRef.position.y > position.y):
			velocity.y += 1
		elif (playerRef.position.y < position.y):
			velocity.y -= 1	
	
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		$AnimatedSprite.play()
		
	if velocity.x != 0:
		$AnimatedSprite.animation = "run"
		$AnimatedSprite.flip_v = false
		$AnimatedSprite.flip_h = velocity.x < 0
	else:
		$AnimatedSprite.animation = "default"
		
	
	position += velocity * delta	
	position.x = clamp(position.x, 32, screen_size.x - 32)
	position.y = clamp(position.y, 32, screen_size.y - 64)

func stun(duration: = 0.3)->void:
	stun_timer.wait_time = duration
	stun_timer.start()

func knock_back(direction: Vector2)->void:
	var distance = bounceEffect
	tween.interpolate_property(self, "position", position, position + direction * distance, 0.3, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.start()
	
func checkEnemyLife(duration: = 1.0, damaged: = 10)->void:
	$LifeBar.visible = true
	life_timer.wait_time = duration
	life_timer.start()	
	$LifeBar.value -= damaged
	if ($LifeBar.value <= 0):
		queue_free()

func _on_Enemy_area_entered(area: Area2D)->void:
	if area.name == 'Player' || area.name == 'Gate':
		var knock_direction = (position - area.position).normalized()
		checkEnemyLife()
		stun()
		knock_back(knock_direction)


func _on_LifeTimer_timeout():
	$LifeBar.visible = false
